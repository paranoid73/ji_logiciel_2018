﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AsanDB;

namespace ji_logiciel
{
    public partial class mainForm : Form
    {
        public static Db dbconn = null;

        public mainForm()
        {
            InitializeComponent();
        }

        //pour manipuler la connexion à MYSQL
        public static Db getCo()
        {
            if(dbconn == null)
            {
                dbconn = new Db();
            }
            return dbconn;
        }


        private void btnMenu_Click(object sender, EventArgs e)
        {
            if(sideBar.Width == 50)
            {
                sideBar.Width = 260;
            }
            else
            {
                sideBar.Width = 50;
            }
        }

        private void bunifuFlatButton4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            tabRandom1.BringToFront();
            tabRandom1.LoadData();
            bunifuFlatButton1.Normalcolor = Color.CornflowerBlue;
            bunifuFlatButton2.Normalcolor = Color.SteelBlue;
            bunifuFlatButton3.Normalcolor = Color.SteelBlue;
            bunifuFlatButton5.Normalcolor = Color.SteelBlue;
        }

        private void bunifuFlatButton5_Click(object sender, EventArgs e)
        {
            tabResultat1.BringToFront();
            bunifuFlatButton1.Normalcolor = Color.SteelBlue;
            bunifuFlatButton2.Normalcolor = Color.SteelBlue;
            bunifuFlatButton3.Normalcolor = Color.SteelBlue;
            bunifuFlatButton5.Normalcolor = Color.CornflowerBlue;
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            tabParrains1.BringToFront();
            tabParrains1.RefreshData();
            bunifuFlatButton1.Normalcolor = Color.SteelBlue;
            bunifuFlatButton2.Normalcolor = Color.CornflowerBlue;
            bunifuFlatButton3.Normalcolor = Color.SteelBlue;
            bunifuFlatButton5.Normalcolor = Color.SteelBlue;
        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            tabFilleul1.BringToFront();
            tabFilleul1.RefreshData();
            bunifuFlatButton1.Normalcolor = Color.SteelBlue;
            bunifuFlatButton2.Normalcolor = Color.SteelBlue;
            bunifuFlatButton3.Normalcolor = Color.CornflowerBlue;
            bunifuFlatButton5.Normalcolor = Color.SteelBlue;
        }

        private void btnMaxWindow_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

      

        private void secondForm_Load(object sender, EventArgs e)
        {

        }
    }
}
