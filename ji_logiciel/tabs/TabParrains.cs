﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AsanDB;
using System.IO;

namespace ji_logiciel
{
    public partial class TabParrains : UserControl
    {
        public TabParrains()
        {
            InitializeComponent();
        }

        public async Task<DataTable> GetDataAsync()
        {
            var dt = new DataTable();
            Db db = mainForm.getCo();
            await Task.Run(() => { dt = db.query("select nomPers,prenomPers,numPers,imgPers FROM personnes WHERE niveau > 3"); });
            return dt;
        }

        public async void RefreshData()
        {
            this.DataParrain.DataSource = await GetDataAsync();
        }

        private void DataParrain_SelectionChanged(object sender, EventArgs e)
        {
            DataGridViewCell cell = null;
            foreach (DataGridViewCell selectedCell in DataParrain.SelectedCells)
            {
                cell = selectedCell;
                break;
            }
            if (cell != null)
            {
                DataGridViewRow row = cell.OwningRow;
                tbPrenom.Text = row.Cells["prenom"].Value.ToString();
                tbNom.Text = row.Cells["nom"].Value.ToString();
                tbNumero.Text = row.Cells["num"].Value.ToString();
                var ImageLink = row.Cells["img"].Value;
                try
                {
                    FileStream fs = new System.IO.FileStream(@"..\..\photos\"+ImageLink, FileMode.Open, FileAccess.Read);
                    photoParrain.Image = Image.FromStream(fs);
                    fs.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}
