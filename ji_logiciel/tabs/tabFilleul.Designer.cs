﻿namespace ji_logiciel
{
    partial class tabFilleul
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.tbPrenom = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbNom = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.photoFilleul = new System.Windows.Forms.PictureBox();
            this.DataFilleul = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prenom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.img = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.tbNumero = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            ((System.ComponentModel.ISupportInitialize)(this.photoFilleul)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataFilleul)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(30, 191);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(694, 35);
            this.bunifuSeparator1.TabIndex = 18;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Bebas Neue", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(30, 79);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(74, 29);
            this.bunifuCustomLabel2.TabIndex = 16;
            this.bunifuCustomLabel2.Text = "Prénom";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Bebas Neue", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(30, 32);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(46, 29);
            this.bunifuCustomLabel1.TabIndex = 15;
            this.bunifuCustomLabel1.Text = "Nom";
            // 
            // tbPrenom
            // 
            this.tbPrenom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbPrenom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbPrenom.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbPrenom.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPrenom.Font = new System.Drawing.Font("Roboto", 12F);
            this.tbPrenom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbPrenom.HintForeColor = System.Drawing.Color.Empty;
            this.tbPrenom.HintText = "Prénom";
            this.tbPrenom.isPassword = false;
            this.tbPrenom.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbPrenom.LineIdleColor = System.Drawing.Color.Gray;
            this.tbPrenom.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbPrenom.LineThickness = 3;
            this.tbPrenom.Location = new System.Drawing.Point(165, 69);
            this.tbPrenom.Margin = new System.Windows.Forms.Padding(4);
            this.tbPrenom.MaxLength = 32767;
            this.tbPrenom.Name = "tbPrenom";
            this.tbPrenom.Size = new System.Drawing.Size(286, 39);
            this.tbPrenom.TabIndex = 14;
            this.tbPrenom.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbNom
            // 
            this.tbNom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbNom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbNom.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbNom.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNom.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbNom.HintForeColor = System.Drawing.Color.Empty;
            this.tbNom.HintText = "Nom";
            this.tbNom.isPassword = false;
            this.tbNom.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbNom.LineIdleColor = System.Drawing.Color.Gray;
            this.tbNom.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbNom.LineThickness = 3;
            this.tbNom.Location = new System.Drawing.Point(165, 22);
            this.tbNom.Margin = new System.Windows.Forms.Padding(4);
            this.tbNom.MaxLength = 32767;
            this.tbNom.Name = "tbNom";
            this.tbNom.Size = new System.Drawing.Size(286, 39);
            this.tbNom.TabIndex = 13;
            this.tbNom.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // photoFilleul
            // 
            this.photoFilleul.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.photoFilleul.Location = new System.Drawing.Point(534, 3);
            this.photoFilleul.Name = "photoFilleul";
            this.photoFilleul.Size = new System.Drawing.Size(190, 168);
            this.photoFilleul.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.photoFilleul.TabIndex = 12;
            this.photoFilleul.TabStop = false;
            // 
            // DataFilleul
            // 
            this.DataFilleul.AllowUserToAddRows = false;
            this.DataFilleul.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DataFilleul.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.DataFilleul.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataFilleul.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataFilleul.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.DataFilleul.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataFilleul.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataFilleul.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DataFilleul.ColumnHeadersHeight = 35;
            this.DataFilleul.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nom,
            this.prenom,
            this.num,
            this.img});
            this.DataFilleul.DoubleBuffered = true;
            this.DataFilleul.EnableHeadersVisualStyles = false;
            this.DataFilleul.HeaderBgColor = System.Drawing.Color.DodgerBlue;
            this.DataFilleul.HeaderForeColor = System.Drawing.Color.White;
            this.DataFilleul.Location = new System.Drawing.Point(30, 232);
            this.DataFilleul.MultiSelect = false;
            this.DataFilleul.Name = "DataFilleul";
            this.DataFilleul.ReadOnly = true;
            this.DataFilleul.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DataFilleul.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataFilleul.Size = new System.Drawing.Size(694, 130);
            this.DataFilleul.TabIndex = 11;
            this.DataFilleul.SelectionChanged += new System.EventHandler(this.DataFilleul_SelectionChanged);
            // 
            // nom
            // 
            this.nom.DataPropertyName = "nomPers";
            this.nom.HeaderText = "Nom";
            this.nom.Name = "nom";
            this.nom.ReadOnly = true;
            // 
            // prenom
            // 
            this.prenom.DataPropertyName = "prenomPers";
            this.prenom.HeaderText = "prenom";
            this.prenom.Name = "prenom";
            this.prenom.ReadOnly = true;
            // 
            // num
            // 
            this.num.DataPropertyName = "numPers";
            this.num.HeaderText = "Contact";
            this.num.Name = "num";
            this.num.ReadOnly = true;
            // 
            // img
            // 
            this.img.DataPropertyName = "imgPers";
            this.img.HeaderText = "nom de l\'image";
            this.img.Name = "img";
            this.img.ReadOnly = true;
            this.img.Visible = false;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Bebas Neue", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(30, 129);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(78, 29);
            this.bunifuCustomLabel3.TabIndex = 20;
            this.bunifuCustomLabel3.Text = "Contact";
            // 
            // tbNumero
            // 
            this.tbNumero.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbNumero.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbNumero.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbNumero.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNumero.Font = new System.Drawing.Font("Roboto", 12F);
            this.tbNumero.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbNumero.HintForeColor = System.Drawing.Color.Empty;
            this.tbNumero.HintText = "numero de téléphone";
            this.tbNumero.isPassword = false;
            this.tbNumero.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbNumero.LineIdleColor = System.Drawing.Color.Gray;
            this.tbNumero.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbNumero.LineThickness = 3;
            this.tbNumero.Location = new System.Drawing.Point(165, 119);
            this.tbNumero.Margin = new System.Windows.Forms.Padding(4);
            this.tbNumero.MaxLength = 32767;
            this.tbNumero.Name = "tbNumero";
            this.tbNumero.Size = new System.Drawing.Size(286, 39);
            this.tbNumero.TabIndex = 19;
            this.tbNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tabFilleul
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.bunifuCustomLabel3);
            this.Controls.Add(this.tbNumero);
            this.Controls.Add(this.bunifuSeparator1);
            this.Controls.Add(this.bunifuCustomLabel2);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.tbPrenom);
            this.Controls.Add(this.tbNom);
            this.Controls.Add(this.photoFilleul);
            this.Controls.Add(this.DataFilleul);
            this.Name = "tabFilleul";
            this.Size = new System.Drawing.Size(759, 400);
            ((System.ComponentModel.ISupportInitialize)(this.photoFilleul)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataFilleul)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbPrenom;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbNom;
        private System.Windows.Forms.PictureBox photoFilleul;
        private Bunifu.Framework.UI.BunifuCustomDataGrid DataFilleul;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbNumero;
        private System.Windows.Forms.DataGridViewTextBoxColumn nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn prenom;
        private System.Windows.Forms.DataGridViewTextBoxColumn num;
        private System.Windows.Forms.DataGridViewTextBoxColumn img;
    }
}
