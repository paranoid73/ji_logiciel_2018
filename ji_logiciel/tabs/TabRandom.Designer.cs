﻿namespace ji_logiciel
{
    partial class TabRandom
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TabRandom));
            this.Nom1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Nom2 = new System.Windows.Forms.Label();
            this.picAnimation = new System.Windows.Forms.PictureBox();
            this.btnNext = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnPrev = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnResStart = new Bunifu.Framework.UI.BunifuThinButton2();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.ovalPictureBox2 = new ji_logiciel.OvalPictureBox();
            this.ovalPictureBox1 = new ji_logiciel.OvalPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picAnimation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ovalPictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ovalPictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Nom1
            // 
            this.Nom1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Nom1.AutoSize = true;
            this.Nom1.Font = new System.Drawing.Font("Roboto Lt", 48F);
            this.Nom1.ForeColor = System.Drawing.Color.Orange;
            this.Nom1.Location = new System.Drawing.Point(-10, 432);
            this.Nom1.MaximumSize = new System.Drawing.Size(300, 0);
            this.Nom1.Name = "Nom1";
            this.Nom1.Size = new System.Drawing.Size(292, 154);
            this.Nom1.TabIndex = 2;
            this.Nom1.Text = "NOM PRENOM";
            this.Nom1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Nom2
            // 
            this.Nom2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Nom2.AutoSize = true;
            this.Nom2.Font = new System.Drawing.Font("Roboto Lt", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nom2.ForeColor = System.Drawing.Color.Orange;
            this.Nom2.Location = new System.Drawing.Point(738, 432);
            this.Nom2.MaximumSize = new System.Drawing.Size(300, 300);
            this.Nom2.Name = "Nom2";
            this.Nom2.Size = new System.Drawing.Size(292, 154);
            this.Nom2.TabIndex = 3;
            this.Nom2.Text = "NOM PRENOM";
            this.Nom2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // picAnimation
            // 
            this.picAnimation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picAnimation.Image = global::ji_logiciel.Properties.Resources.animation;
            this.picAnimation.Location = new System.Drawing.Point(345, 28);
            this.picAnimation.Name = "picAnimation";
            this.picAnimation.Size = new System.Drawing.Size(356, 401);
            this.picAnimation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picAnimation.TabIndex = 11;
            this.picAnimation.TabStop = false;
            this.picAnimation.Visible = false;
            // 
            // btnNext
            // 
            this.btnNext.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnNext.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.ImageActive = null;
            this.btnNext.Location = new System.Drawing.Point(614, 519);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(101, 89);
            this.btnNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnNext.TabIndex = 10;
            this.btnNext.TabStop = false;
            this.btnNext.Zoom = 10;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnPrev.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrev.Image = ((System.Drawing.Image)(resources.GetObject("btnPrev.Image")));
            this.btnPrev.ImageActive = null;
            this.btnPrev.Location = new System.Drawing.Point(302, 512);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(88, 96);
            this.btnPrev.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnPrev.TabIndex = 9;
            this.btnPrev.TabStop = false;
            this.btnPrev.Visible = false;
            this.btnPrev.Zoom = 10;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnResStart
            // 
            this.btnResStart.ActiveBorderThickness = 1;
            this.btnResStart.ActiveCornerRadius = 40;
            this.btnResStart.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.btnResStart.ActiveForecolor = System.Drawing.Color.White;
            this.btnResStart.ActiveLineColor = System.Drawing.Color.Transparent;
            this.btnResStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResStart.BackColor = System.Drawing.Color.White;
            this.btnResStart.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnResStart.BackgroundImage")));
            this.btnResStart.ButtonText = "RECOMMENCER";
            this.btnResStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnResStart.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResStart.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnResStart.IdleBorderThickness = 1;
            this.btnResStart.IdleCornerRadius = 40;
            this.btnResStart.IdleFillColor = System.Drawing.Color.Yellow;
            this.btnResStart.IdleForecolor = System.Drawing.Color.Black;
            this.btnResStart.IdleLineColor = System.Drawing.Color.Yellow;
            this.btnResStart.Location = new System.Drawing.Point(411, 628);
            this.btnResStart.Margin = new System.Windows.Forms.Padding(5);
            this.btnResStart.Name = "btnResStart";
            this.btnResStart.Size = new System.Drawing.Size(195, 50);
            this.btnResStart.TabIndex = 8;
            this.btnResStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnResStart.Click += new System.EventHandler(this.btnResStart_Click);
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.ForeColor = System.Drawing.Color.Gainsboro;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(447, 3);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(124, 540);
            this.bunifuSeparator1.TabIndex = 14;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = true;
            // 
            // ovalPictureBox2
            // 
            this.ovalPictureBox2.BackColor = System.Drawing.Color.White;
            this.ovalPictureBox2.Image = global::ji_logiciel.Properties.Resources.user_logo;
            this.ovalPictureBox2.Location = new System.Drawing.Point(3, 28);
            this.ovalPictureBox2.Name = "ovalPictureBox2";
            this.ovalPictureBox2.Size = new System.Drawing.Size(321, 342);
            this.ovalPictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ovalPictureBox2.TabIndex = 13;
            this.ovalPictureBox2.TabStop = false;
            // 
            // ovalPictureBox1
            // 
            this.ovalPictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ovalPictureBox1.BackColor = System.Drawing.Color.White;
            this.ovalPictureBox1.Image = global::ji_logiciel.Properties.Resources.user_logo;
            this.ovalPictureBox1.Location = new System.Drawing.Point(704, 28);
            this.ovalPictureBox1.Name = "ovalPictureBox1";
            this.ovalPictureBox1.Size = new System.Drawing.Size(323, 345);
            this.ovalPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ovalPictureBox1.TabIndex = 12;
            this.ovalPictureBox1.TabStop = false;
            // 
            // TabRandom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.picAnimation);
            this.Controls.Add(this.bunifuSeparator1);
            this.Controls.Add(this.ovalPictureBox2);
            this.Controls.Add(this.ovalPictureBox1);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.Nom2);
            this.Controls.Add(this.Nom1);
            this.Controls.Add(this.btnResStart);
            this.Name = "TabRandom";
            this.Size = new System.Drawing.Size(1030, 683);
            this.Load += new System.EventHandler(this.TabRandom_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picAnimation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ovalPictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ovalPictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuCustomLabel Nom1;
        private System.Windows.Forms.Label Nom2;
        private Bunifu.Framework.UI.BunifuThinButton2 btnResStart;
        private Bunifu.Framework.UI.BunifuImageButton btnPrev;
        private Bunifu.Framework.UI.BunifuImageButton btnNext;
        private System.Windows.Forms.PictureBox picAnimation;
        private OvalPictureBox ovalPictureBox1;
        private OvalPictureBox ovalPictureBox2;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
    }
}
