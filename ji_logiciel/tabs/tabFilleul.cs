﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AsanDB;
using System.IO;
using System.Drawing.Imaging;

namespace ji_logiciel
{
    public partial class tabFilleul : UserControl
    {
        public tabFilleul()
        {
            InitializeComponent();
        }

        public async Task<DataTable> GetDataAsync()
        {
            var dt = new DataTable();
            Db db = mainForm.getCo();
            await Task.Run(() => { dt = db.query("select nomPers,prenomPers,numPers,imgPers FROM personnes WHERE niveau <= 2"); });
            return dt;
        }


        public async void RefreshData()
        {
            this.DataFilleul.DataSource = await GetDataAsync();
        }
        

        private void DataFilleul_SelectionChanged(object sender, EventArgs e)
        {
            DataGridViewCell cell = null;
            foreach (DataGridViewCell selectedCell in DataFilleul.SelectedCells)
            {
                cell = selectedCell;
                break;
            }
            if (cell != null)
            {
                DataGridViewRow row = cell.OwningRow;
                tbPrenom.Text = row.Cells["prenom"].Value.ToString();
                tbNom.Text = row.Cells["nom"].Value.ToString();
                tbNumero.Text = row.Cells["num"].Value.ToString();
                var ImageLink = row.Cells["img"].Value;
                try
                {
                    FileStream fs = new System.IO.FileStream(@"..\..\photos\"+ImageLink, FileMode.Open, FileAccess.Read);
                    photoFilleul.Image = Image.FromStream(fs);
                    fs.Close();
                }catch(Exception ex)
                {
                    
                }
            }
        }
    }
}
