﻿namespace ji_logiciel
{
    partial class TabParrains
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DataParrain = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.photoParrain = new System.Windows.Forms.PictureBox();
            this.tbNom = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbPrenom = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.tbNumero = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prenom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.img = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DataParrain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.photoParrain)).BeginInit();
            this.SuspendLayout();
            // 
            // DataParrain
            // 
            this.DataParrain.AllowUserToAddRows = false;
            this.DataParrain.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DataParrain.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataParrain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataParrain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataParrain.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.DataParrain.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataParrain.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataParrain.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataParrain.ColumnHeadersHeight = 25;
            this.DataParrain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nom,
            this.prenom,
            this.numero,
            this.img});
            this.DataParrain.DoubleBuffered = true;
            this.DataParrain.EnableHeadersVisualStyles = false;
            this.DataParrain.HeaderBgColor = System.Drawing.Color.DodgerBlue;
            this.DataParrain.HeaderForeColor = System.Drawing.Color.White;
            this.DataParrain.Location = new System.Drawing.Point(30, 232);
            this.DataParrain.MultiSelect = false;
            this.DataParrain.Name = "DataParrain";
            this.DataParrain.ReadOnly = true;
            this.DataParrain.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DataParrain.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DataParrain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataParrain.Size = new System.Drawing.Size(694, 130);
            this.DataParrain.TabIndex = 0;
            this.DataParrain.SelectionChanged += new System.EventHandler(this.DataParrain_SelectionChanged);
            // 
            // photoParrain
            // 
            this.photoParrain.Location = new System.Drawing.Point(521, 21);
            this.photoParrain.Name = "photoParrain";
            this.photoParrain.Size = new System.Drawing.Size(190, 168);
            this.photoParrain.TabIndex = 1;
            this.photoParrain.TabStop = false;
            // 
            // tbNom
            // 
            this.tbNom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbNom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbNom.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbNom.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNom.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbNom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbNom.HintForeColor = System.Drawing.Color.Empty;
            this.tbNom.HintText = "Nom";
            this.tbNom.isPassword = false;
            this.tbNom.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbNom.LineIdleColor = System.Drawing.Color.Gray;
            this.tbNom.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbNom.LineThickness = 3;
            this.tbNom.Location = new System.Drawing.Point(165, 21);
            this.tbNom.Margin = new System.Windows.Forms.Padding(4);
            this.tbNom.MaxLength = 32767;
            this.tbNom.Name = "tbNom";
            this.tbNom.Size = new System.Drawing.Size(286, 39);
            this.tbNom.TabIndex = 2;
            this.tbNom.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbPrenom
            // 
            this.tbPrenom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbPrenom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbPrenom.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbPrenom.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPrenom.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbPrenom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbPrenom.HintForeColor = System.Drawing.Color.Empty;
            this.tbPrenom.HintText = "Prénom";
            this.tbPrenom.isPassword = false;
            this.tbPrenom.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbPrenom.LineIdleColor = System.Drawing.Color.Gray;
            this.tbPrenom.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbPrenom.LineThickness = 3;
            this.tbPrenom.Location = new System.Drawing.Point(165, 68);
            this.tbPrenom.Margin = new System.Windows.Forms.Padding(4);
            this.tbPrenom.MaxLength = 32767;
            this.tbPrenom.Name = "tbPrenom";
            this.tbPrenom.Size = new System.Drawing.Size(286, 39);
            this.tbPrenom.TabIndex = 3;
            this.tbPrenom.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(30, 36);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(51, 24);
            this.bunifuCustomLabel1.TabIndex = 5;
            this.bunifuCustomLabel1.Text = "Nom";
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(30, 83);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(77, 24);
            this.bunifuCustomLabel2.TabIndex = 7;
            this.bunifuCustomLabel2.Text = "Prénom";
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(30, 190);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(694, 35);
            this.bunifuSeparator1.TabIndex = 9;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(30, 144);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(73, 24);
            this.bunifuCustomLabel3.TabIndex = 11;
            this.bunifuCustomLabel3.Text = "Contact";
            // 
            // tbNumero
            // 
            this.tbNumero.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbNumero.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbNumero.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbNumero.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNumero.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbNumero.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbNumero.HintForeColor = System.Drawing.Color.Empty;
            this.tbNumero.HintText = "numero de téléphone";
            this.tbNumero.isPassword = false;
            this.tbNumero.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbNumero.LineIdleColor = System.Drawing.Color.Gray;
            this.tbNumero.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbNumero.LineThickness = 3;
            this.tbNumero.Location = new System.Drawing.Point(165, 129);
            this.tbNumero.Margin = new System.Windows.Forms.Padding(4);
            this.tbNumero.MaxLength = 32767;
            this.tbNumero.Name = "tbNumero";
            this.tbNumero.Size = new System.Drawing.Size(286, 39);
            this.tbNumero.TabIndex = 10;
            this.tbNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // nom
            // 
            this.nom.DataPropertyName = "nomPers";
            this.nom.HeaderText = "Nom";
            this.nom.Name = "nom";
            this.nom.ReadOnly = true;
            // 
            // prenom
            // 
            this.prenom.DataPropertyName = "prenomPers";
            this.prenom.HeaderText = "Prénom";
            this.prenom.Name = "prenom";
            this.prenom.ReadOnly = true;
            // 
            // numero
            // 
            this.numero.DataPropertyName = "numeroPers";
            this.numero.HeaderText = "Numero";
            this.numero.Name = "numero";
            this.numero.ReadOnly = true;
            // 
            // img
            // 
            this.img.DataPropertyName = "imgPers";
            this.img.HeaderText = "nom de l\'image";
            this.img.Name = "img";
            this.img.ReadOnly = true;
            // 
            // TabParrains
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.bunifuCustomLabel3);
            this.Controls.Add(this.tbNumero);
            this.Controls.Add(this.bunifuSeparator1);
            this.Controls.Add(this.bunifuCustomLabel2);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.tbPrenom);
            this.Controls.Add(this.tbNom);
            this.Controls.Add(this.photoParrain);
            this.Controls.Add(this.DataParrain);
            this.Name = "TabParrains";
            this.Size = new System.Drawing.Size(739, 400);
            ((System.ComponentModel.ISupportInitialize)(this.DataParrain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.photoParrain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCustomDataGrid DataParrain;
        private System.Windows.Forms.PictureBox photoParrain;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbNom;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbPrenom;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbNumero;
        private System.Windows.Forms.DataGridViewTextBoxColumn nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn prenom;
        private System.Windows.Forms.DataGridViewTextBoxColumn numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn img;
    }
}
