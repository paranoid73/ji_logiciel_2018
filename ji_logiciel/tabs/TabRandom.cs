﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AsanDB;
using System.Threading;

namespace ji_logiciel
{
    public partial class TabRandom : UserControl
    {
        /* le nombre de tour */
        int Iteration = 0;
        bool fin = false;
        int lastid = 0;
        DataTable Filleuls;
        DataTable Parrains;

        /*liste des identifiants */
        List<int> FilleulListId;
        List<int> ParrainListId;

        /*Liste temporaires */
        List<int> ListA = new List<int>();
        List<int> listB = new List<int>();
        List<int> listPrev = new List<int>();

        public TabRandom()
        {
            InitializeComponent();
        }

        /*
            * charger les données des filleules et des parrains dans des datatable
            */
        public async Task GetDataAsync()
        {
            Db db = mainForm.getCo();
            await Task.Run(() =>
            {
                Filleuls = db.query("SELECT idPers as id , CONCAT(nomPers ,' ',prenomPers) as nom , imgPers as img FROM personnes WHERE niveau <= 2 ");
                Parrains = db.query("SELECT idPers as id , CONCAT(nomPers ,' ',prenomPers) as nom , imgPers as img FROM personnes WHERE niveau >= 3");

                /* changer une liste complète de leurs identifiants */
                ParrainListId = Parrains.AsEnumerable().Select(p => p.Field<int>("id")).ToList();
                FilleulListId = Filleuls.AsEnumerable().Select(p => p.Field<int>("id")).ToList();
            });
        }

        /*
            * Charger les données
            */
        public async void LoadData()
        {
            picAnimation.Show();
            picAnimation.Update();
            try
            {
                await GetDataAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERREUR " + ex);
            }
            picAnimation.Hide();
        }

        /*Pour le random */
        public async Task<Tuple<int, int>> RandomSelect()
        {
            int indexP = 0;
            int indexF = 0;
            ++Iteration;
            await Task.Run(() =>
            {

                int NbreParrains = ParrainListId.Count;
                int NbreFilleuls = FilleulListId.Count;

                /* si le nomnbre de parrain est superieur à inf à l'iteration alors on recharger la liste des parrains */
                if (NbreParrains == ListA.Count)
                {
                    ListA.Clear();
                }

                /* random index Parrain */
                Random random = new Random();
                do
                {
                    indexP = random.Next(0, NbreParrains);
                } while (ListA.Contains(indexP));

                if (listB.Count == FilleulListId.Count)
                {
                    fin = true;
                }
                else
                {
                    /* random index Filleuls */
                    do
                    {
                        indexF = random.Next(0, NbreFilleuls);
                    } while (listB.Contains(indexF));
                }

                /* retirer les identifiants des listes */
                ListA.Add(indexP);
                listB.Add(indexF);

                for (int i = 0; i < 100000000; i++) ;

            });
            return new Tuple<int, int>(indexP, indexF);
        }

        private async void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                ++Iteration;
                /*  debut animation gif */
                picAnimation.Show();
                picAnimation.Update();

                /* donnée reçu du random */
                var data = await RandomSelect();
                int P = data.Item1;
                int F = data.Item2;


                if (fin == true)
                {
                    btnNext.Visible = false;
                    ovalPictureBox1.Image = Properties.Resources.user_logo;
                    Nom1.Text = "NOM PRENOM";
                    ovalPictureBox2.Image = Properties.Resources.user_logo;
                    Nom2.Text = "NOM PRENOM";
                }
                else
                {
                    /* mettre à jour l'interface graphique */
                    /* parrains */
                    var nomImg1 = Parrains.AsEnumerable().Where(p => p.Field<int>("id") == ParrainListId[P]).Select(p => p.Field<string>("img")).FirstOrDefault();
                    Nom1.Text = Parrains.AsEnumerable().Where(p => p.Field<int>("id") == ParrainListId[P]).Select(p => p.Field<string>("nom")).FirstOrDefault();
                    ovalPictureBox2.Image = Image.FromFile(@"..\..\photos\" + nomImg1);

                    /* filleuls */
                    var nomImg2 = Filleuls.AsEnumerable().Where(p => p.Field<int>("id") == FilleulListId[F]).Select(p => p.Field<string>("img")).FirstOrDefault();
                    Nom2.Text = Filleuls.AsEnumerable().Where(p => p.Field<int>("id") == FilleulListId[F]).Select(p => p.Field<string>("nom")).FirstOrDefault();
                    ovalPictureBox1.Image = Image.FromFile(@"..\..\photos\" + nomImg2);
                }

                /* fin animation gif */
                picAnimation.Hide();

                if (Iteration > 2)
                {
                    btnPrev.Visible = true;
                }
                /* affichage du bouton précédent après avoir cliquer une première fois sur le bouton */

                /* enregistrement dans la base de donnée */
                Db db = mainForm.getCo();
                string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                db.bind(new string[] { "a", ParrainListId[P].ToString(), "b", FilleulListId[F].ToString(), "c", date });
                int created = db.nQuery("INSERT INTO `parrainage` (`idParrain`, `idFilleul`, `date`) VALUES(@a,@b,@c)");
                lastid = Convert.ToInt32(db.single("SELECT LAST_INSERT_ID()"));
                listPrev.Add(lastid);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERREUR " + ex);
            }
        }

        private void TabRandom_Load(object sender, EventArgs e)
        {
            LoadData();
        }



        private void btnPrev_Click(object sender, EventArgs e)
        {
            Db db = mainForm.getCo();
            string[] ids = db.row("SELECT idFilleul , idParrain FROM parrainage WHERE id = "+ lastid);
            string[] p = db.row("SELECT  CONCAT(nomPers, ' ', prenomPers) as nom, imgPers as img FROM personnes WHERE  idPers = "+ids[0]);
            string[] f = db.row("SELECT  CONCAT(nomPers, ' ', prenomPers) as nom, imgPers as img FROM personnes WHERE  idPers = " + ids[1]);
            /**/
            Nom1.Text = p[0];
            ovalPictureBox2.Image = Image.FromFile(@"..\..\photos\" + p[1]);

            /* filleuls */
            Nom2.Text = f[0];
            ovalPictureBox1.Image = Image.FromFile(@"..\..\photos\" + f[1]);
        }

        private void btnResStart_Click(object sender, EventArgs e)
        {
            ListA.Clear();
            listB.Clear();
            Iteration = 0;
            btnNext.Visible = true;
            btnPrev.Visible = false;
            ovalPictureBox1.Image = Properties.Resources.user_logo;
            Nom1.Text = "NOM PRENOM";
            ovalPictureBox2.Image = Properties.Resources.user_logo;
            Nom2.Text = "NOM PRENOM";
            fin = false;
        }
    }
}
