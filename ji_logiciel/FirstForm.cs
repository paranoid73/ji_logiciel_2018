﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ji_logiciel
{
    public partial class FirstForm : Form
    {
        public FirstForm()
        {
            InitializeComponent();

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            mainForm sc = new mainForm();
            sc.Show();
            this.Hide();
        }

        private void closeLb_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
