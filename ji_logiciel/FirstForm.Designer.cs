﻿namespace ji_logiciel
{
    partial class FirstForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FirstForm));
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.topBar = new System.Windows.Forms.Panel();
            this.TopleftTitleLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuTransition1 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.btnStart = new Bunifu.Framework.UI.BunifuThinButton2();
            this.topLeftLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.YearLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.TitleLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.closeLb = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.topBar.SuspendLayout();
            this.bunifuGradientPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.topBar;
            this.bunifuDragControl1.Vertical = true;
            // 
            // topBar
            // 
            resources.ApplyResources(this.topBar, "topBar");
            this.topBar.BackColor = System.Drawing.Color.Transparent;
            this.topBar.Controls.Add(this.closeLb);
            this.topBar.Controls.Add(this.TopleftTitleLabel);
            this.bunifuTransition1.SetDecoration(this.topBar, BunifuAnimatorNS.DecorationType.None);
            this.topBar.Name = "topBar";
            // 
            // TopleftTitleLabel
            // 
            resources.ApplyResources(this.TopleftTitleLabel, "TopleftTitleLabel");
            this.TopleftTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.TopleftTitleLabel, BunifuAnimatorNS.DecorationType.None);
            this.TopleftTitleLabel.ForeColor = System.Drawing.Color.White;
            this.TopleftTitleLabel.Name = "TopleftTitleLabel";
            // 
            // bunifuTransition1
            // 
            this.bunifuTransition1.AnimationType = BunifuAnimatorNS.AnimationType.HorizBlind;
            this.bunifuTransition1.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.bunifuTransition1.DefaultAnimation = animation1;
            this.bunifuTransition1.AllAnimationsCompleted += new System.EventHandler(this.btnStart_Click);
            // 
            // bunifuGradientPanel1
            // 
            resources.ApplyResources(this.bunifuGradientPanel1, "bunifuGradientPanel1");
            this.bunifuGradientPanel1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuGradientPanel1.Controls.Add(this.topBar);
            this.bunifuGradientPanel1.Controls.Add(this.btnStart);
            this.bunifuGradientPanel1.Controls.Add(this.topLeftLabel2);
            this.bunifuGradientPanel1.Controls.Add(this.YearLabel);
            this.bunifuGradientPanel1.Controls.Add(this.TitleLabel);
            this.bunifuTransition1.SetDecoration(this.bunifuGradientPanel1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.SystemColors.HotTrack;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.DarkBlue;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.DodgerBlue;
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            // 
            // btnStart
            // 
            resources.ApplyResources(this.btnStart, "btnStart");
            this.btnStart.ActiveBorderThickness = 1;
            this.btnStart.ActiveCornerRadius = 65;
            this.btnStart.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.btnStart.ActiveForecolor = System.Drawing.Color.White;
            this.btnStart.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.btnStart.BackColor = System.Drawing.Color.Transparent;
            this.btnStart.ButtonText = "C O M M EN C E R";
            this.btnStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.btnStart, BunifuAnimatorNS.DecorationType.None);
            this.btnStart.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnStart.IdleBorderThickness = 1;
            this.btnStart.IdleCornerRadius = 65;
            this.btnStart.IdleFillColor = System.Drawing.Color.Yellow;
            this.btnStart.IdleForecolor = System.Drawing.Color.Black;
            this.btnStart.IdleLineColor = System.Drawing.Color.Yellow;
            this.btnStart.Name = "btnStart";
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // topLeftLabel2
            // 
            resources.ApplyResources(this.topLeftLabel2, "topLeftLabel2");
            this.topLeftLabel2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.topLeftLabel2, BunifuAnimatorNS.DecorationType.None);
            this.topLeftLabel2.ForeColor = System.Drawing.Color.White;
            this.topLeftLabel2.Name = "topLeftLabel2";
            // 
            // YearLabel
            // 
            resources.ApplyResources(this.YearLabel, "YearLabel");
            this.YearLabel.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.YearLabel, BunifuAnimatorNS.DecorationType.None);
            this.YearLabel.ForeColor = System.Drawing.Color.White;
            this.YearLabel.Name = "YearLabel";
            // 
            // TitleLabel
            // 
            resources.ApplyResources(this.TitleLabel, "TitleLabel");
            this.TitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.TitleLabel, BunifuAnimatorNS.DecorationType.None);
            this.TitleLabel.ForeColor = System.Drawing.Color.Yellow;
            this.TitleLabel.Name = "TitleLabel";
            // 
            // closeLb
            // 
            resources.ApplyResources(this.closeLb, "closeLb");
            this.closeLb.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.closeLb, BunifuAnimatorNS.DecorationType.None);
            this.closeLb.ForeColor = System.Drawing.Color.White;
            this.closeLb.Name = "closeLb";
            this.closeLb.Click += new System.EventHandler(this.closeLb_Click);
            // 
            // FirstForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bunifuGradientPanel1);
            this.bunifuTransition1.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FirstForm";
            this.topBar.ResumeLayout(false);
            this.topBar.PerformLayout();
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.bunifuGradientPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuCustomLabel YearLabel;
        private Bunifu.Framework.UI.BunifuCustomLabel TopleftTitleLabel;
        private Bunifu.Framework.UI.BunifuCustomLabel topLeftLabel2;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private Bunifu.Framework.UI.BunifuThinButton2 btnStart;
        private System.Windows.Forms.Panel topBar;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition1;
        private Bunifu.Framework.UI.BunifuCustomLabel TitleLabel;
        private Bunifu.Framework.UI.BunifuCustomLabel closeLb;
    }
}

